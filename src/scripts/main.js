/**
 * Charts library.
 * Generates SVG charts in D3, the easy way.
 * Dependencies: D3.js
 * Notes: Currently only supports donut and radar charts.
 */
var Charts = {
  radar: Radar,
  donut: Donut
};

exports.Charts = Charts;