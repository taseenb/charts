module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    browserSync: {
      bsFiles: {
        src: ['./src/**', './dist/index.html']
      },
      options: {
        watchTask: true,
        server: "./dist"
      }
    },

    watch: {
      sass: {
        files: './src/styles/**',
        tasks: ['sass']
      },
      js: {
        files: './src/scripts/**',
        tasks: ['buildScripts']
      }
    },

    sass: {
      dist: {
        options: {
          style: 'expanded',
          sourcemap: 'none'
        },
        files: [{
          expand: true,     // Enable dynamic expansion.
          cwd: './src/styles/',      // Src matches are relative to this path.
          src: ['*.scss'], // Actual pattern(s) to match.
          dest: './dist/css/',   // Destination path prefix.
          ext: '.css',   // Dest filepaths will have this extension.
          // extDot: 'first'   // Extensions in filenames begin after the first dot
        }],
      },
    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        sourceMap: true
      },
      build: {
        src: './dist/js/charts.js',
        dest: './dist/js/<%= pkg.name %>.min.js'
      }
    },

    // babel: {
    //   options: {
    //     sourceMap: true,
    //     presets: ['es2015']
    //   },
    //   dist: {
    //     files: {
    //       './dist/js/<%= pkg.name %>.min.js': './src/scripts/main.js'
    //     }
    //   }
    // }

    concat: {
      options: {
        // separator: ';',
        banner: '(function(exports, document, undefined){\n',
        footer: '\n})(window, document, undefined)'
      },
      dist: {
        src: [
          // Utils
          'src/scripts/utils.js',

          // ## Chart views ##
          // Chart view - Donut
          'src/scripts/views/donut.js',

          // Chart view - Radar
          'src/scripts/views/radar/radar-chart.js',
          'src/scripts/views/radar.js',

          // Main
          'src/scripts/main.js'
        ],
        dest: 'dist/js/charts.js',
      },
    },

  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  // grunt.loadNpmTasks('grunt-babel');
  // grunt.loadNpmTasks('grunt-browserify');


  grunt.registerTask('buildScripts', ['concat', 'uglify']);
  grunt.registerTask('build', ['buildScripts', 'sass']);
  grunt.registerTask('default', ['build', 'browserSync', 'watch']);

};