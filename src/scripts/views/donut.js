function Donut(options) {

  var o = options || {};
  var defaultOptions = {
    color: '#ff0000',
    size: 20,
    data: {a: 1, b: 2, c: 3}
  };

  this.options = extend(defaultOptions, o);

  console.log(this.options);
}

Donut.prototype = {};