# Charts

On a besoin d'une librairie pour créer les différentes charts correspondant aux maquettes. On a testée plusieurs libs existantes (Chart.js, Chartist.js) mais aucune ne permet suffisamment de personnalisation pour arriver au résultat voulu. D3.js est sûrement l'outil adapté mais nous ne connaissons pas bien la librairie et n'avons pas le temps de nous former dessus.

- On veut un rendu SVG et non canvas.
- Pas de problématique responsive, ce sera affiché uniquement sur iPad iOS Safari >= 7.
- Il faut prévoir la possibilité de gérer des effets de hover en CSS et des animations à la création des charts.

## Donut chart

Il faut que chaque label hérite de la couleur du slice de donut qui lui correspond.
Les labels doivent être des liens cliquables et on doit pouvoir leur passer une URL.

Proposition d'interface :

```js
Charts.donut({
  el: '#chart-donut-clean-data',
  labels: [
    {
      title: 'Dialight', // le script doit extraire la valeur correspondante dans les datas et l'ajouter après dans un <b></b>
      subtitle: 'National average: <b>14%</b>',
      path: '#/salon/301033/brand/dialight'
    },
    // ...
  ],
  data: [
    {
      name: 'Dialight',
      value: 20,
      color: '#87bbe1'
    },
    // ...
  ]
});
```

- Si une entrée vaut moins de 10, elle n'est pas affiché dans le donut mais en dessous avec sa valeur à côté.
- Si une entrée vaut 0, elle n'est pas affichée dans le donut mais en dessous et sans valeur à côté.
- Si toutes les entrées valent 0, on n'affiche pas le donut mais seulement les entrées.

## Radar chart

Proposition d'interface :

```js
Charts.radar({
  el: '#chart-radar-clean-data',
  divisions: 6, // le nombre de divisions dans le radar
  labels: [
    {
      title: 'Cool Tones',
      color: '#3ebaf5',
      maxValue: 40
    }
    // ...
  ],
  data: [
    {
      name: 'Cool Tones',
      value: 14,
      color: '#745ff2' // ça sera la même couleur pour tous les points et les traits au sein d'un même radar
    }
    // ...
  ]
});
```

- Pour ce chart, pas de comportement particulier pour les valeurs faibles/nulles.